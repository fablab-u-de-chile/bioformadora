# Bioformadora

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Bioformadora es una termoformadora que funciona con calor refractario o vapor de agua para moldear láminas de biomateriales sobre figuras sólidas. El flujo de trabajo se inicia con la [Biomixer](https://gitlab.com/fablab-u-de-chile/biomixer), donde se generan mezclas precisas que luego se vierten en bastidores con el tamaño necesario. La lámina debe estar deshidratada al momento de termoformar y no superar los 400x400mm.  

Este proyecto es parte de la iniciativa [Nodo de BioFabricación Digital](https://gitlab.com/fablab-u-de-chile/NBD) liderada por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional Materiom, y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile.

<img src="img/img2.jpg" width="400"> <img src="/img/img1.jpg" width="400">

## Atributos

- Termoformadora con calefactores cerámicos de 400W
- Modulo de vaporización - Vapoformadora
- Pantalla TFT 3.5'' como interfaz de usuario
- Arduino UNO como controlador
- Bomba de vacio 1 HP

<img src="img/IMG_0614.JPG" width="600">

## Como construirla

- [Listado](docs/parts.md) de partes, piezas y componentes
- [Partes CAD](cad/)
- [Sistema Electrónico](docs/elec.md)


## Trabajo Futuro

En cuanto a la tecnología, estamos integrando el sistema de vapoformado y agregando manual de control de la interfaz gráfica. En cuanto a los resultados del proceso, actualmente nos encontramos integrando el sistema de vaporizado y estamos trabajando para definir las fórmulas más adecuadas para obtener láminas idóneas para termoformar, en cuanto a espesor y consistencia.  

## Contáctanos!

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

## Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution-ShareAlike 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
